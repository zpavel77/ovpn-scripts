#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3
import hashlib
import sys
conn = sqlite3.connect('comps.sqlite')
c = conn.cursor()
#Функция занесения пользователя в базу
def add_user(username,userpass,ips,keydir):
    c.execute("INSERT INTO users (name,password,enabled,ipaddress,keydir) VALUES ('%s','%s',1,'%s','%s')"%(username,userpass,ips,keydir))
    conn.commit()
#Вводим данные
name = sys.argv[1]
passwd = sys.argv[2]
ips = sys.argv[3]
h=hashlib.md5(passwd)
hpasswd=str(h.hexdigest())
print('\n')
#Делаем запрос в базу
print("Список пользователей:\n")
keypath='/mnt/keys/'+name
add_user(name,hpasswd,ips,keypath)
c.execute('SELECT * FROM users')
row = c.fetchone()
#выводим список пользователей в цикле
while row is not None:
   print row
#   print("id:"+str(row[0])+" Логин: "+row[1]+" | Пароль: "+str(row[2]))
   row = c.fetchone()
# закрываем соединение с базой
c.close()
conn.close()
# addovpn-user
import subprocess
cmd = '/etc/openvpn/easy-rsa/2.0/addovpn-user '+name
subprocess.Popen(cmd, shell = True)
#make ccd
f=open('/etc/openvpn/ccd/'+name,'w',)
ips2 =''
s=ips

dig1=s.split('.')
n=int(dig1[3])
n=n+1
dig1[3]=str(n)
s2='.'
ips2=s2.join(dig1)

f.write('ifconfig-push '+ips+' '+ips2)
f.close
