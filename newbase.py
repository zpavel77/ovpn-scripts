#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3
#Подключение к базе
conn = sqlite3.connect('comps.sqlite')
#Создание курсора
c = conn.cursor()
#Создание таблицы
c.execute('''CREATE TABLE users (id INTEGER  primary key AUTOINCREMENT not null,name varchar, password varchar, enabled int,ipaddress varchar,keydir varchar )''')
#Наполнение таблицы
c.execute("INSERT INTO users (id,name,password,enabled,ipaddress,keydir) VALUES (1,'admin','123',1,'no ip-s','no keys')")
#Подтверждение отправки данных в базу
conn.commit()
#Завершение соединения
c.close()
conn.close()
